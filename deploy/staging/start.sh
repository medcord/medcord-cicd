#!/bin/bash

echo -e "\n*************************"
echo -e "***  GETTING UPDATES  ***"
echo -e "*************************"
cd ~/projects
if [ "$1" = "prod" ]
then
  cd medcord/medcord-backend
  echo -e "\nUpdating backend production...\n"
  ssh-agent bash -c 'ssh-add ~/.ssh/id_rsa; git pull'
fi
if [ "$1" = "dev" ]
then
  cd medcord/medcord-backend-dev
  
  echo -e "\nUpdating backend development...\n"
  ssh-agent bash -c 'ssh-add ~/.ssh/id_rsa; git pull'
  rm -rf src/database/migrations/*.ts
fi
cd ..


if [ "$1" = "dev" ]
then
	cd ~/projects/
	echo -e "\n**********************"
	echo -e "* Running backend... *"
	echo -e "**********************"
	docker compose build app-medcord-dev
	sleep 1
	docker compose up -d app-medcord-dev
	sleep 1
  docker exec -t app-medcord-dev sh -c "npm run migrations:generate -- migration"
  docker exec -t app-medcord-dev sh -c "npm run migrations:run"
fi
	
if [ "$1" = "prod" ]
then
	cd ~/projects/
	echo -e "\n***********************"
	echo -e "*  Running service... *"
	echo -e "***********************"
	docker compose build app-medcord
	sleep 1
	docker compose up -d app-medcord
fi
