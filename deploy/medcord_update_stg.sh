#!/bin/bash

##################### CLEANING PREVIOUS ENVIRONMENT ######################
ssh -T -i "./alaorden.pem" -p 22 -oStrictHostKeyChecking=no ubuntu@${IP_STAGING} <<EOF
rm -dfr ~/staging
EOF

echo -e "\n***********************"
echo -e "* SENDING ALL SCRIPTS *"
echo -e "***********************"
scp -i "./alaorden.pem" -P 22 -oStrictHostKeyChecking=no -r staging ubuntu@${IP_STAGING}:~/

########################### STARTING SERVICES ###########################
if [ "$1" = "prod" ]
then
	ssh -T -i "./alaorden.pem" -p 22 -oStrictHostKeyChecking=no ubuntu@${IP_STAGING} <<EOF
source ~/staging/start.sh prod
EOF
fi

if [ "$1" = "dev" ]
then
	ssh -T -i "./alaorden.pem" -p 22 -oStrictHostKeyChecking=no ubuntu@${IP_STAGING} <<EOF
source ~/staging/start.sh dev
EOF
fi

echo -e "Done"
