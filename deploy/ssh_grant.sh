#!/bin/bash

echo -e "Installing SSH...\n"
apt-get update -y && apt-get install openssh-client -y

echo -e "\nKeypair for ec2 instance...\n"
eval $(ssh-agent -s)
echo "$AWS_KEY_PAIR" > alaorden.pem
chmod 0400 alaorden.pem
ssh-add alaorden.pem
